---
layout: post
title:  "JS打印HTML页面"
date:   2018-03-27  +0800
categories: jekyll update
tags: JS
excerpt: 调用js打印html页面。
---
#### 简单的一行代码调用
        <script>
            function printHtml(){
            window.print();
            }
        </script>
        <button onclick="printHtml();">打印</button>


#### 打印指定区域
        <script>
            function appointprint(){
            var newWindow=window.open("打印窗口","_blank");
            var docStr=document.getElementById('appoint').innerHTML;
            newWindow.document.write(docStr);
            newWindow.print();
            newWindow.close();
            }
        </script>
        <button onclick="appointprint()">打印指定区域</button>
