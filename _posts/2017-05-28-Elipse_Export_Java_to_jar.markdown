---
layout: post
title: "Eclipse将Java项目打包成jar"
date:   2017-05-28 +0800
categories: jekyll update
tags: Java
excerpt: 本篇文章主要讲解利用Eclipse将Java项目打包成jar
---
#### 1.右键项目>EXport..>Java>Runnable JAR file>next
#### 2.选择要打包的项目和主文件，要打包成的文件名，finish
![](/img/Eclipse01.png)
